<?php

use App\Helpers\QuestionnarieStatuses;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable(false);

            //step 1
            $table->string('name')->nullable(true);
            $table->string('surname')->nullable(true);
            $table->string('patronymic')->nullable(true);
            $table->integer('city_id')->nullable(true);
            $table->string('phone')->nullable(true);
            $table->string('email')->nullable(true);
            $table->date('birthday')->nullable(true);
            $table->boolean('nationality_ru')->nullable(true);

            //step 2
            $table->integer('health_status')->nullable(true);
            $table->string('diseases')->nullable(true);

            //step 3
            $table->string('education_type')->nullable(true);
            $table->string('education_institution')->nullable(true);
            $table->string('education_degree')->nullable(true);

            //step 4
            $table->string('work_company')->nullable(true);
            $table->string('work_position')->nullable(true);
            $table->string('work_schelude')->nullable(true);

            //step 5
            $table->string('interests')->nullable(true);

            //step 6
            $table->string('reason')->nullable(true);
            $table->integer('child_expectations_age')->nullable(true);
            $table->boolean('child_expectations_sex')->nullable(true);
            $table->string('visits_frequency')->nullable(true);
            $table->string('bad_habbits_alcohol')->nullable(true);
            $table->string('bad_habbits_smoke')->nullable(true);
            $table->string('bad_habbits_drugs')->nullable(true);

            $table->integer('step')->default(0);
            $table->enum('status', QuestionnarieStatuses::all())->default(QuestionnarieStatuses::DRAFT);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
