<?php

use App\Helpers\Roles;
use App\Models\User;
use App\Services\UsersService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(UsersService $usersService)
    {
        //administrators
        $usersService->create([
            'email' => 'senior_curator@gmail.com',
            'password' => 123123123,
            'name' => 'Иван',
            'city_id' => 1,
            'role' => Roles::SENIOR_CURATOR,
        ]);
        $usersService->create([
            'email' => 'senior_psy@gmail.com',
            'password' => 123123123,
            'name' => 'Никита',
            'city_id' => 1,
            'role' => Roles::SENIOR_PSYCHOLOGIST,
        ]);
        $usersService->create([
            'email' => 'curator_1@gmail.com',
            'password' => 123123123,
            'name' => 'Наталья',
            'city_id' => 1,
            'role' => Roles::CURATOR,
        ]);
        $usersService->create([
            'email' => 'psy_1@gmail.com',
            'password' => 123123123,
            'name' => 'Никита',
            'city_id' => 1,
            'role' => Roles::PSYCHOLOGIST,
        ]);
        $usersService->create([
            'email' => 'curator_2@gmail.com',
            'password' => 123123123,
            'name' => 'Наталья',
            'city_id' => 173,
            'role' => Roles::CURATOR,
        ]);
        $usersService->create([
            'email' => 'psy_2@gmail.com',
            'password' => 123123123,
            'name' => 'Оксана',
            'city_id' => 173,
            'role' => Roles::PSYCHOLOGIST,
        ]);

        $usersService->createWithQuestionnaire([
            'email' => 'user_1@gmail.com',
            'password' => 123123123,
            'name' => 'Инокентий',
            'city_id' => 1,
            'role' => Roles::USER,
            'birthday' => '11.03.1991'
        ]);

        $usersService->createWithQuestionnaire([
            'email' => 'mentor_1@gmail.com',
            'password' => 123123123,
            'name' => 'Виктор',
            'city_id' => 1,
            'role' => Roles::MENTOR,
            'birthday' => '11.03.1991'
        ]);
    }
}
