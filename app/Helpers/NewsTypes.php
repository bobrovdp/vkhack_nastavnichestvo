<?php


namespace App\Helpers;


class NewsTypes
{

    const RECOMENDATION = 'recomendation';

    const NEWS = 'news';

    const TRAINING = 'training';

    static $names = [
        self::RECOMENDATION => 'Мероприятия в городе',
        self::NEWS => 'Новости проекта',
        self::TRAINING => 'Учебные мероприятия',
    ];

    /**
     * Вернет алиасы всех ролей пользователя
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::RECOMENDATION,
            self::NEWS,
            self::TRAINING
        ];
    }

    public static function names()
    {
        return [
            self::RECOMENDATION => self::$names[self::RECOMENDATION],
            self::NEWS => self::$names[self::NEWS],
            self::TRAINING => self::$names[self::TRAINING],
        ];
    }

    /**
     * Вернет название роли по алиасу
     *
     * @param string $alias
     *
     * @return mixed|null
     */
    public static function getName(string $alias)
    {
        return self::$names[$alias] ?? null;
    }
}
