<?php


namespace App\Helpers;


class MessagesTypes
{
    const TEXT = 'text';
    const IMAGE = 'image';
    const REPORT = 'report';

    static $names = [
        self::TEXT => 'Текстовое сообщение',
        self::IMAGE => 'Изображение',
        self::REPORT => 'Отчёт',
    ];

    /**
     * Вернет алиасы всех ролей пользователя
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::TEXT,
            self::IMAGE,
            self::REPORT
        ];
    }

    /**
     * Вернет название роли по алиасу
     *
     * @param string $alias
     *
     * @return mixed|null
     */
    public static function getName(string $alias)
    {
        return self::$names[$alias] ?? null;
    }
}