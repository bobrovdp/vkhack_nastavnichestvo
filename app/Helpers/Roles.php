<?php

namespace App\Helpers;

class Roles
{
    const USER = 'user';

    const MENTOR = 'mentor';

    const CURATOR = 'curator';

    const PSYCHOLOGIST = 'psychologist';

    const SENIOR_CURATOR = 'senior_curator';

    const SENIOR_PSYCHOLOGIST = 'senior_psychologist';

    static $names = [
        self::USER => 'Кандидат',
        self::MENTOR => 'Наставник',
        self::CURATOR => 'Куратор',
        self::PSYCHOLOGIST => 'Психолог',
        self::SENIOR_CURATOR => 'Старший куратор',
        self::SENIOR_PSYCHOLOGIST => 'Старший психолог',
    ];

    public static function admin()
    {
        return [
            self::CURATOR,
            self::PSYCHOLOGIST
        ];
    }

    public static function superAdmin()
    {
        return [
            self::SENIOR_CURATOR,
            self::SENIOR_PSYCHOLOGIST,
        ];
    }

    /**
     * Вернет алиасы всех ролей пользователя
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::USER,
            self::MENTOR,
            self::CURATOR,
            self::PSYCHOLOGIST,
            self::SENIOR_CURATOR,
            self::SENIOR_PSYCHOLOGIST,
        ];
    }

    public static function names()
    {
        return [
            self::USER => self::$names[self::USER],
            self::MENTOR => self::$names[self::MENTOR],
            self::CURATOR => self::$names[self::CURATOR],
            self::PSYCHOLOGIST => self::$names[self::PSYCHOLOGIST],
            self::SENIOR_CURATOR => self::$names[self::SENIOR_CURATOR],
            self::SENIOR_PSYCHOLOGIST => self::$names[self::SENIOR_PSYCHOLOGIST],
        ];
    }

    public static function mentornNames()
    {
        return [
            self::USER => self::$names[self::USER],
            self::MENTOR => self::$names[self::MENTOR],
        ];
    }

    public static function adminNames()
    {
        return [
            self::CURATOR => self::$names[self::CURATOR],
            self::PSYCHOLOGIST => self::$names[self::PSYCHOLOGIST],
        ];
    }

    public static function superAdminNames()
    {
        return [
            self::SENIOR_CURATOR => self::$names[self::SENIOR_CURATOR],
            self::SENIOR_PSYCHOLOGIST => self::$names[self::SENIOR_PSYCHOLOGIST],
        ];
    }

    /**
     * Вернет название роли по алиасу
     *
     * @param string $alias
     *
     * @return mixed|null
     */
    public static function getName(string $alias)
    {
        return self::$names[$alias] ?? null;
    }
}
