<?php

namespace App\Helpers;

class QuestionnarieStatuses
{
    const UNDER_REVIEW = 'under_review';

    const APPROVED = 'approved';

    const REJECTED = 'rejected';

    const DRAFT = 'draft';

    static $names = [
        self::DRAFT => 'Черновик',
        self::UNDER_REVIEW => 'На рассмотрении',
        self::APPROVED => 'Подтверждён',
        self::REJECTED => 'Отклонён'
    ];

    public static function names()
    {
        return [
            self::DRAFT => self::$names[self::DRAFT],
            self::UNDER_REVIEW => self::$names[self::UNDER_REVIEW],
            self::APPROVED => self::$names[self::APPROVED],
            self::REJECTED => self::$names[self::REJECTED],
        ];
    }

    /**
     * Вернет алиасы всех ролей пользователя
     *
     * @return array
     */
    public static function all()
    {
        return [
            self::DRAFT,
            self::UNDER_REVIEW,
            self::APPROVED,
            self::REJECTED
        ];
    }

    /**
     * Вернет название роли по алиасу
     *
     * @param string $alias
     *
     * @return mixed|null
     */
    public static function getName(string $alias)
    {
        return self::$names[$alias] ?? null;
    }
}
