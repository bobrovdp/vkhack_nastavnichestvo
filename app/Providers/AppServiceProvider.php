<?php

namespace App\Providers;

use App\Services\NewsService;
use App\Services\ReferenceService;
use App\Services\ReportsService;
use App\Services\UsersService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \DB::enableQueryLog();

        $this->app->singleton(UsersService::class, function() {
            return new UsersService();
        });

        $this->app->singleton(ReferenceService::class, function() {
            return new ReferenceService();
        });

        $this->app->singleton(ReportsService::class, function () {
            return new ReportsService();
        });

        $this->app->singleton(NewsService::class, function() {
            return new NewsService();
        });
    }
}
