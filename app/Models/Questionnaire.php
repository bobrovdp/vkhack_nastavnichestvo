<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $table = 'questionnaires';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'patronymic',
        'city_id',
        'phone',
        'email',
        'birthday',
        'nationality_ru',
        'health_status',
        'diseases',
        'education_type',
        'education_institution',
        'education_degree',
        'work_company',
        'work_position',
        'work_schelude',
        'interests',
        'reason',
        'child_expectations',
        'visits_frequency',
        'bad_habbits_alcohol',
        'bad_habbits_smoke',
        'bad_habbits_drugs',
        'step',
        'status'
    ];

    protected $casts = [
        'dates' => 'json',
        'rating_children_before' => 'integer',
        'rating_children_after' => 'integer',
        'rating_after' => 'integer',
        'photos' => 'json'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
