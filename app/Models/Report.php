<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = [
        'dates',
        'description',
        'rating_children_before',
        'rating_children_after',
        'rating_after',
        'target',
        'comment',
        'photos'
    ];

    protected $casts = [
        'dates' => 'json',
        'rating_children_before' => 'integer',
        'rating_children_after' => 'integer',
        'rating_after' => 'integer',
        'photos' => 'json'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getPhotosAttribute($value)
    {
        if (empty($value)) {
            return null;
        }

        return array_map(function($file) {
            return Storage::url($file);
        }, json_decode($value, true));
    }
}
