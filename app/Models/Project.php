<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Project extends Model
{
    protected $table = 'projects';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'image',
        'city_id',
    ];

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function getImageAttribute($value)
    {
        if (empty($value)) {
            return null;
        }

        return Storage::url($value);
    }
}
