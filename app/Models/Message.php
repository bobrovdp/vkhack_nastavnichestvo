<?php

namespace App\Models;

use App\Helpers\MessagesTypes;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'to',
        'from',
        'type',
        'content'
    ];

    public function getContentAttribute($value)
    {
        switch ($this->type) {
        case MessagesTypes::TEXT:
            return $value;
            break;
        case MessagesTypes::REPORT:
            return Report::find((int)$value);
        }
        return $value;
    }

    public function getCreatedAtAttribute($value)
    {
        if (strtotime($value) < strtotime('today')) {
            return date('d.m', strtotime($value));
        } else {
            return date('H:i', strtotime($value));
        }
    }
}
