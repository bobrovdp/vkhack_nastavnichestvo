<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'name',
        'type',
        'image_path',
        'roles',
        'city_ids',
        'text',
    ];

    protected $casts = [
        'roles' => 'json',
        'city_ids' => 'json',
    ];

    public function getCityesAttribute()
    {
        if (empty($this->city_ids)) {
            return null;
        }

        return City::find($this->city_ids);
    }

    public function getImagePathAttribute($value)
    {
        if (empty($value)) {
            return null;
        }

        return Storage::url($value);
    }
}
