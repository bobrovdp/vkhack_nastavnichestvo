<?php

namespace App\Models;

use App\Helpers\Roles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'name',
        'role',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute($value)
    {
        if ($this->isAdmin() || $this->isSuperAdmin()) {
            return $value;
        }
        return $this->profile->name;
    }

    public function findForPassport($username)
    {
        return $this->where('email', $username)->first();
    }

    public function references()
    {
        return $this->hasMany(Reference::class, 'user_id', 'id');
    }

    public function reports()
    {
        return $this->hasMany(Report::class, 'user_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne(Questionnaire::class, 'user_id', 'id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function isAdmin()
    {
        return in_array($this->role, Roles::admin());
    }

    public function isSuperAdmin()
    {
        return in_array($this->role, Roles::superAdmin());
    }
}
