<?php

namespace App\Services;

use App\Helpers\QuestionnarieStatuses;
use App\Models\Questionnaire;
use App\Models\User;

class QuestionnarieService
{
    public function query()
    {
        return Questionnaire::query();
    }

    public function approve(Questionnaire $questionnaire)
    {
        return $questionnaire->update([
            'status' => QuestionnarieStatuses::APPROVED
        ]);
    }

    public function reject(Questionnaire $questionnaire)
    {
        return $questionnaire->update([
            'status' => QuestionnarieStatuses::REJECTED
        ]);
    }

    public function under_review(Questionnaire $questionnaire)
    {
        return $questionnaire->update([
            'status' => QuestionnarieStatuses::UNDER_REVIEW
        ]);
    }

    public function update(Questionnaire $questionnaire, array $data)
    {
        if (!empty($data['status']) && !in_array($data['status'], QuestionnarieStatuses::all())) {
            throw new \Exception('Неверный статус анкеты!');
        }

        return $questionnaire->update($data);
    }

    public function getCurrent(User $user): Questionnaire
    {
        return Questionnaire::firstOrCreate([
            'user_id' => $user->getKey()
        ]);
    }

    public function getById($id): Questionnaire
    {
        return Questionnaire::find($id);
    }

    public function formatToOutput(Questionnaire $q): array
    {
        $rows = $q->toArray();
        $result = [];
        foreach ($rows as $row => $value) {
            $key = null;
            switch ($row) {
                case 'surname':
                case 'name':
                case 'patronymic':
                case 'city_id':
                case 'phone':
                case 'email':
                case 'birthday':
                case 'nationality_ru':
                    $key = 0;
                    break;
                case 'health_status':
                case 'diseases':
                    $key = 1;
                    break;
                case 'education_type':
                case 'education_institution':
                case 'education_degree':
                    $key = 2;
                    break;
                case 'work_company':
                case 'work_position':
                case 'work_schelude':
                    $key = 3;
                    break;
                case 'interests':
                    $key = 4;
                    break;
                case 'reason':
                case 'child_expectations_age':
                case 'child_expectations_sex':
                case 'visits_frequency':
                case 'bad_habbits_alcohol':
                case 'bad_habbits_smoke':
                case 'bad_habbits_drugs':
                    $key = 5;
                    break;
            default:
                $result[$row] = $value;
                break;
            }
            if (!is_null($key)) {
                $result['questionnaire'][$key][$row] = $value;
            }
        }
        return $result;
    }

    public function formatFromOutput(array $data): array
    {
        $result = [];
        foreach ($data as $key => $value) {
            if ($key === 'questionnaire') {
                foreach ($value as $i => $v) {
                    foreach ($v as $a => $b) {
                        $result[$a] = $b;
                    }
                }
            }
            if (is_numeric($key) && is_array($value)) {
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}
