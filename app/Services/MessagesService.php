<?php


namespace App\Services;


use App\Models\Message;
use App\Models\User;

class MessagesService
{
    public function getListFrom(User $user, $id = null)
    {
        return Message::query()->when(!empty($id), function ($query) use ($user) {
            $query->where('to', '=', $user->getKey());
        })->orderBy('id', 'DESC')->limit(10)->get();
    }
}