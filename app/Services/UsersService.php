<?php

namespace App\Services;

use App\Helpers\QuestionnarieStatuses;
use App\Helpers\Roles;
use App\Models\User;

class UsersService
{
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query():\Illuminate\Database\Eloquent\Builder
    {
        return User::query();
    }
    /**
     * Создает пользователя
     *
     * @param array $data
     *
     * @return User
     */
    public function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);

        return User::create($data);
    }
    /**
     * Обновляет пользователя
     *
     * @param array $data
     *
     * @return User
     */
    public function update(User $user, array $data)
    {
        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        return $user;
    }

    public function createWithQuestionnaire(array $data)
    {
        $user = $this->create(array_except($data, ['name', 'birthday']));
        $questionnaireService = new QuestionnarieService();
        $questionnaire = $questionnaireService->getCurrent($user);
        if ($user->role === Roles::MENTOR) {
            $data['status'] = QuestionnarieStatuses::APPROVED;
        }
        $questionnaire->update(array_except($data, ['password', 'role']));
    }
}
