<?php

namespace App\Services;

use App\Models\Project;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProjectsService
{
    const STORE_PATH = 'public/projects';
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query():\Illuminate\Database\Eloquent\Builder
    {
        return Project::query();
    }

    public function create(array $data, ?UploadedFile $image)
    {
        if (! empty($image)) {
            $data['image'] = $image->store(self::STORE_PATH);
        }

        return Project::create($data);
    }
}
