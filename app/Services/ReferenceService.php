<?php

namespace App\Services;

use App\Models\Reference;
use App\Models\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ReferenceService
{

    const STORE_PATH = 'public/references';
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query():\Illuminate\Database\Eloquent\Builder
    {
        return Reference::query();
    }

    public function create(User $user, UploadedFile $file, $type):Reference
    {
        $path = $file->store(self::STORE_PATH);

        $reference = new Reference([
           'path' => $path,
           'type' => $type,
           'status' => 'send'
       ]);

        return $user->references()->save($reference);
    }
}
