<?php

namespace App\Services;

use App\Models\Report;
use App\Models\User;

class ReportsService
{

    const STORE_PATH = 'public/reports';
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query():\Illuminate\Database\Eloquent\Builder
    {
        return Report::query();
    }

    /**
     * @param User $user
     * @param array $data
     * @param array $files
     *
     * @return Report
     */
    public function create(User $user, array $data, array $files)
    {
        $files = array_map(function ($file) {
            return $file->store(self::STORE_PATH);
        }, $files);

        $report = new Report([
            'dates' => $data['dates'],
            'description' => $data['description'],
            'rating_children_before' => $data['rating_children_before'],
            'rating_children_after' => $data['rating_children_after'],
            'rating_after' => $data['rating_after'],
            'target' => $data['target'],
            'comment' => $data['comment'],
            'photos' => !empty($files) ? $files : null
        ]);

        return $user->reports()->save($report);
    }
}
