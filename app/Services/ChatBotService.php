<?php


namespace App\Services;


use GuzzleHttp\Client;

class ChatBotService
{
    protected $clientId = 'test_user';

    protected $api = 'https://bot.aimylogic.com/chatapi/';
    protected $key = 'iADNfwLg:10500506dae98671cb1c91af35dae1c091ecff94';

    protected $client;

    public function __construct(string $clientId = null)
    {
        $this->clientId = $clientId ?: $this->clientId;

        $this->client = new Client();
    }

    public function getMessage($query): string
    {
        return json_encode([
            'clientId' => $this->clientId,
            'query' => $query
        ]);
    }

    public function ask(string $query = ''): string
    {
        $message = $this->getMessage($query);

        $body = $this->sendRequest($message);

        return $body['data']['answer'];
    }

    public function sendRequest($message): array
    {
        $response = $this->client->post($this->api . $this->key,
            [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $message
            ]
        );

        return json_decode((string) $response->getBody(), true);
    }
}