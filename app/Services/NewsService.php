<?php

namespace App\Services;

use App\Models\News;
use App\Models\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class NewsService
{
    const STORE_PATH = '/public/news';
    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query():\Illuminate\Database\Eloquent\Builder
    {
        return News::query();
    }

    public function getQueryAcceptByUser(User $user)
    {
        $query = $this->query();

        $query->where(function($query) use ($user) {
            $query->whereNull('roles')
                ->orWhere('roles', '@>', \DB::raw('\'["' . $user->role . '"]\'::jsonb'));
        });

        $query->where(function($query) use ($user) {
            $query->whereNull('city_ids')
                ->orWhere('city_ids', '@>', \DB::raw('\'["' . $user->city_id . '"]\'::jsonb'));
        });

        return $query;
    }

    public function create(array $data, ?UploadedFile $image)
    {
        if (! empty($image)) {
            $data['image_path'] = $image->store(self::STORE_PATH);
        }

        return News::create($data);
    }
}
