<?php

namespace App\Http\Requests\News;

use App\Helpers\NewsTypes;
use App\Helpers\Roles;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'type' => 'required|in:' . implode(',', NewsTypes::all()),
            'image' => 'nullable|image',
            'roles' => 'nullable|array',
            'roles.*' => 'string|in:' . implode(',', Roles::all()),
            'city_ids' => 'nullable|array',
            'city_ids.*' => 'integer|exists:cities,id',
            'text' => 'required|string',
        ];
    }
}
