<?php

namespace App\Http\Requests\Reports;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dates' => 'required|array',
            'dates.*' => 'required|date',
            'description' => 'required|string',
            'rating_children_before' => 'required|integer',
            'rating_children_after' => 'required|integer',
            'rating_after' => 'required|integer',
            'target' => 'required|string',
            'comment' => 'nullable|string',
            'photos' => 'array|nullable',
            'photos.*' => 'image'
        ];
    }
}
