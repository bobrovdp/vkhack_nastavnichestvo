<?php

namespace App\Http\Requests\Questionnarie;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\QuestionnarieStatuses;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:' . implode(',', QuestionnarieStatuses::all()),
        ];
    }
}
