<?php

namespace App\Http\Requests\Questionnarie;

use App\Helpers\QuestionnarieStatuses;
use App\Services\UsersService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(UsersService $usersService)
    {
        return [
            'questionnaire.0.name' => 'string',
            'questionnaire.0.surname' => 'string',
            'questionnaire.0.patronymic' => 'string',
            'questionnaire.0.city_id' => Rule::exists('cities', 'id')->where(function ($query) use ($usersService) {
                return $query->whereIn('id', $usersService->query()
                    ->select('city_id')
                    ->whereNotNull('city_id')
                );
            }),
            'questionnaire.0.phone' => 'string',
            'questionnaire.0.email' => 'email',
            'questionnaire.0.birthday' => 'date',
            'questionnaire.0.nationality_ru' => 'boolean',
            'questionnaire.1.health_status' => 'integer',
            'questionnaire.1.diseases' => 'string',
            'questionnaire.2.education_type' => 'string',
            'questionnaire.2.education_institution' => 'string',
            'questionnaire.2.education_degree' => 'string',
            'questionnaire.3.work_company' => 'string',
            'questionnaire.3.work_position' => 'string',
            'questionnaire.3.work_schelude' => 'string',
            'questionnaire.4.interests' => 'string',
            'questionnaire.5.reason' => 'string',
            'questionnaire.5.child_expectations_age' => 'integer',
            'questionnaire.5.child_expectations_sex' => 'boolean',
            'questionnaire.5.visits_frequency' => 'string',
            'questionnaire.5.bad_habbits_alcohol' => 'boolean',
            'questionnaire.5.bad_habbits_smoke' => 'boolean',
            'questionnaire.5.bad_habbits_drugs' => 'boolean',
            'step' => 'integer',
            'status' => 'in:' . QuestionnarieStatuses::DRAFT . ',' . QuestionnarieStatuses::UNDER_REVIEW
        ];
    }
}
