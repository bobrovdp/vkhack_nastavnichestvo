<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questionnarie\UpdateRequest;
use App\Services\QuestionnarieService;

class QuestionnaireController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param QuestionnarieService $questionnarieService
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionnarieService $questionnarieService)
    {
        $user = \Auth::user();

        return response()->json(
            $questionnarieService->formatToOutput($questionnarieService->getCurrent($user))
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param QuestionnarieService $questionnarieService
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, QuestionnarieService $questionnarieService)
    {
        $user = $request->user();

        $questionnarieService->update(
            $questionnarieService->getCurrent($user),
            $questionnarieService->formatFromOutput(
                $request->all()
            )
        );
        return response()->json(
            $questionnarieService->formatToOutput(
                $questionnarieService->getCurrent($user)
            )
        );
    }
}
