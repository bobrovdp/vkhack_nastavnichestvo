<?php

namespace App\Http\Controllers;

use App\Helpers\Roles;
use App\Http\Requests\Projects\CreateRequest;
use App\Services\ProjectsService;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ProjectsController extends Controller
{
    use ValidatesRequests;

    public function index(ProjectsService $projectsService)
    {
        $projects = $projectsService->query()->with('city')->get();

        $result = [];

        foreach($projects as $project) {
            if (empty($result[$project->city->getKey()])) {
                $result[$project->city->getKey()] = [
                    'name' => $project->city->name,
                    'id' => $project->city->getKey(),
                    'projects' => []
                ];
            }

            $result[$project->city->getKey()]['projects'][] = $project;
        }

        return response()->json(array_values($result));
    }

    public function create(CreateRequest $request, ProjectsService $projectsService)
    {
        if ($request->user()->role != Roles::SENIOR_MENTOR) {
            return abort(403);
        }

        return response()->json($projectsService->create($request->all(), $request->file('image')));
    }
}
