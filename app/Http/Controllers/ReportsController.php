<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reports\CreateRequest;
use App\Http\Requests\Reports\IndexRequest;
use App\Services\ReportsService;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ReportsController extends Controller
{
    use ValidatesRequests;

    public function index(IndexRequest $request, ReportsService $reportsService)
    {
        $reports = $reportsService->query()
            ->where('user_id', '=', $request->user()->getKey())
            ->get();

        return response()->json([
            'reports' => $reports
        ]);
    }

    public function create(CreateRequest $request, ReportsService $reportsService)
    {
        $this->validate($request, [
            'dates' => 'required|array',
            'dates.*' => 'required|date',
            'description' => 'required|string',
            'rating_children_before' => 'required|integer',
            'rating_children_after' => 'required|integer',
            'rating_after' => 'required|integer',
            'target' => 'required|string',
            'comment' => 'nullable|string',
            'photos' => 'array|nullable',
            'photos.*' => 'image'
        ]);

        return $reportsService->create($request->user(), $request->all(), $request->file('photos'));
    }
}
