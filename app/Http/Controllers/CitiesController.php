<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Services\UsersService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CitiesController extends BaseController
{
    public function index(Request $request, UsersService $usersService)
    {
        if (true == $request->get('is_works')) {
            return response()->json([
                'cities' => City::whereIn('id', $usersService->query()->select('city_id')->whereNotNull('city_id'))->get()
            ]);
        }

        return response()->json([
            'cities' => City::all()
        ]);
    }
}
