<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Services\ReportsService;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ReportsService $reportsService)
    {
        $reports = $reportsService->query()
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('reports.index', [
            'reports' => $reports
        ]);
    }

    public function edit(Report $report)
    {
        return view('reports.edit', [
            'report' => $report
        ]);
    }
}
