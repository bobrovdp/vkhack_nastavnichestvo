<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NewsTypes;
use App\Helpers\Roles;
use App\Http\Controllers\Controller;
use App\Http\Requests\News\CreateRequest;
use App\Models\City;
use App\Models\News;
use App\Services\NewsService;
use App\Services\UsersService;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(NewsService $newsService)
    {
        $news = $newsService->query()
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('news.index', [
            'newses' => $news,
            'types' => NewsTypes::names()
        ]);
    }

    public function create(UsersService $usersService)
    {
        return view('news.create', [
            'types' => NewsTypes::names(),
            'roles' => Roles::names(),
            'city_ids' => City::whereIn('id', $usersService->query()->select('city_id')->whereNotNull('city_id'))->get()
        ]);
    }

    public function store(CreateRequest $request, NewsService $newsService)
    {
        try {
            $newsService->create($request->all(), $request->file('image'));

            return redirect()->route('admin.news.index')->with('message', 'Сохранено успешно!');
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    public function edit(News $news, UsersService $usersService)
    {
        return view('news.edit', [
            'news' => $news,
            'types' => NewsTypes::names(),
            'roles' => Roles::names(),
            'city_ids' => City::whereIn('id', $usersService->query()->select('city_id')->whereNotNull('city_id'))->get()
        ]);
    }
}
