<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Roles;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use App\Services\UsersService;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(UsersService $usersService)
    {
        $users = $usersService->query()
            ->whereNotIn('role', [
                Roles::USER,
                Roles::MENTOR
            ])
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('users.index', [
            'users' => $users
        ]);
    }

    public function edit(User $user)
    {
        return view('users.edit', [
            'user' => $user,
            'roles' => Roles::adminNames() + Roles::superAdminNames(),
            'cities' => City::query()->orderBy('name', 'ASC')->get()->all()
        ]);
    }

    public function create()
    {
        return view('users.create',
            [
                'roles' => Roles::adminNames() + Roles::superAdminNames(),
                'cities' => City::query()->orderBy('name', 'ASC')->get()->all()
            ]
        );
    }

    public function update(Request $request, User $user, UsersService $usersService)
    {
        $usersService->update($user, $request->all());
        return redirect(route('admin.users.index'));
    }

    public function store(Request $request, UsersService $usersService)
    {
        $usersService->create($request->all());

        return redirect(route('admin.users.index'));
    }
}
