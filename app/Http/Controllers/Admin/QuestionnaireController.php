<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\QuestionnarieStatuses;
use App\Http\Controllers\Controller;
use App\Http\Requests\Questionnarie\AdminUpdateRequest;
use App\Models\Questionnaire;
use App\Services\QuestionnarieService;

class QuestionnaireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Questionnaire $q)
    {
        $user = $q->user;
        return view('questionnaire.index', [
            'user' => $user,
            'statuses' => QuestionnarieStatuses::names()
        ]);
    }

    public function edit(Questionnaire $q)
    {
        return view('questionnaire.edit', [
            'q' => $q,
            'statuses' => QuestionnarieStatuses::names()
        ]);
    }

    public function update(AdminUpdateRequest $request, QuestionnarieService $questionnaireService, Questionnaire $q)
    {
        try {
            $questionnaireService->update($q, $request->all());

            return redirect()->route('admin.questionnaire.index')->with('message', 'Сохранено успешно!');
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
