<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Roles;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use App\Services\UsersService;
use Illuminate\Http\Request;

class MentorsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(UsersService $usersService)
    {
        $user = \Auth::user();
        $users = $usersService->query()->with('profile')
            ->when($user->isAdmin(), function ($query) use ($user) {
                $query->join('questionnaires', 'questionnaires.user_id', '=', 'users.id')
                    ->where('questionnaires.city_id', '=', $user->city_id)
                    ->select('users.*');
            })
            ->whereIn('role', [
                Roles::USER,
                Roles::MENTOR
            ])
            ->orderBy('users.id', 'desc')
            ->paginate(15);

        return view('mentors.index', [
            'users' => $users
        ]);
    }

    public function edit(User $user)
    {
        return view('mentors.edit', [
            'user' => $user,
            'roles' => Roles::mentornNames(),
            'cities' => City::query()->orderBy('name', 'ASC')->get()->all()
        ]);
    }

    public function create()
    {
        return view('mentors.create',
            [
                'roles' => Roles::mentornNames(),
                'cities' => City::query()->orderBy('name', 'ASC')->get()->all()
            ]
        );
    }

    public function update(Request $request, User $user, UsersService $usersService)
    {
        $usersService->update($user, $request->all());
        return redirect(route('admin.mentors.index'));
    }

    public function store(Request $request, UsersService $usersService)
    {
        $usersService->create($request->all());

        return redirect(route('admin.mentors.index'));
    }
}
