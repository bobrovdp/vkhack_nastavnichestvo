<?php

namespace App\Http\Controllers;

use App\Models\Reference;
use App\Services\ReferenceService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ReferenceController extends BaseController
{
    use ValidatesRequests;

    public function create(Request $request, ReferenceService $referenceService)
    {
        $this->validate($request, [
            'img' => 'required|image',
            'type' => 'required|in:passport',
        ]);

        $reference = $referenceService->create(\Auth::user(), $request->file('img'), $request->get('type'));

        return response()->json($request);
    }

    public function index(Request $request, ReferenceService $referenceService)
    {
        $user = \Auth::user();

        if (empty($user)) {
            return abort(401);
        }

        return response()->json([
            'references' => $referenceService->query()->where('user_id', '=', $user->getKey())->get()
        ]);
    }
}
