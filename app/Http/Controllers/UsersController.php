<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\QuestionnarieService;
use App\Services\UsersService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;

class UsersController extends BaseController
{
    use ValidatesRequests;

    public function create(Request $request, UsersService $usersService, QuestionnarieService $questionnarieService)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:8',
                'name' => 'required|string',
                'birthday' => 'required|before_or_equal:' . date('d.m.Y', time()-(60*60*24*365*24)),
                'city_id' => [
                    'required',
                    Rule::exists('cities', 'id')->where(function ($query) use ($usersService) {
                        return $query->whereIn('id', $usersService->query()
                            ->select('city_id')
                            ->whereNotNull('city_id')
                        );
                    }),
                ],
            ], [
                'birthday.before_or_equal' => 'Извините вам меньше 24 лет!',
                'city_id.exists' => 'Извините в выбранном городе мы не работаем!',
            ]);

            /**
             * @var User $user
             */
            $user = $usersService->create($request->all());

            $profile = $questionnarieService->getCurrent($user);
            $profile->update([
                'name' => $request->get('name'),
                'birthday' => $request->get('birthday'),
                'city_id' => $request->get('city_id'),
                'email' => $request->get('email')
            ]);

            $token = $user->createToken('application')->accessToken;

            return response()->json($user->toArray() + [
                'access_token' => $token
            ]);
        } catch (ValidationException $exception) {
            return response()->json([
                'success' => false,
                'errors' => $exception->errors()
            ], 403);
        }
    }

    public function apiAuth(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $user = User::where('email', '=', $request->get('email'))->first();

        if (empty($user)) {
            return response()->json([
                'success' => false,
                'errors' => 'Пользователь с такой почтой и паролем не найден!'
            ]);
        }

        if (! Hash::check($request->get('password'), $user->password)) {
            return response()->json([
                'success' => false,
                'errors' => 'Пользователь с такой почтой и паролем не найден!'
            ]);
        }

        $token = $user->createToken('application')->accessToken;

        return response()->json([
            'access_token' => $token
        ]);
    }
}
