<?php

namespace App\Http\Controllers;

use App\Http\Requests\News\IndexRequest;
use App\Http\Requests\News\CreateRequest;
use App\Services\NewsService;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class NewsController extends BaseController
{
    use ValidatesRequests;

    public function index(IndexRequest $request, NewsService $newsService)
    {
        $query = $newsService->getQueryAcceptByUser($request->user())->orderBy('id', 'desc');

        return response()->json([
            'total' => $query->count(),
            'news' => $query->get()
        ]);
    }

    public function create(CreateRequest $request, NewsService $newsService)
    {
        $news = $newsService->create($request->all(), $request->file('image'));

        return response()->json([
            'id' => $news->getKey(),
            'name' => $news->name,
            'type' => $news->type,
            'text' => $news->text,
            'image_path' => $news->image_path,
            'roles' => $news->roles,
            'city_ids' => $news->city_ids,
            'cityes' => $news->cityes,
        ]);
    }
}
