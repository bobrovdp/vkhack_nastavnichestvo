<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user->isAdmin() || $user->isSuperAdmin()) {
            return $next($request);
        }
        return response()->json(['status' => 'error', 'reason' => 'Недостаточно прав'], 403);
    }
}
