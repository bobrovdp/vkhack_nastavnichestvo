@extends('layouts.app')
@section('title')Наставники@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Наставники и кандидаты (@if(\Auth::user()->isSuperAdmin())Все города@else{{\Auth::user()->city->name}}@endif)</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Статус</th>
                            <th>Город</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{ $user->name }}
                                </td>
                                <td>
                                    {{ \App\Helpers\Roles::getName($user->role) }}
                                </td>
                                <td>
                                    {{ $user->profile->city->name }}
                                </td>
                                <td>
                                    <a target="_blank" href="{{ route('admin.questionnaire.index', ['id' => $user->profile->id]) }}">Анкета</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="usersDelete" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="DELETE">
    </form>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
