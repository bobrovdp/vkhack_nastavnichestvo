@extends('layouts.app')
@section('title')События@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">События</h3>
                    <a href="{!! route('admin.news.create') !!}" class="btn btn-primary btn-xs pull-right">Добавить</a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Тип</th>
                            <th>Дата</th>
{{--                            <th></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($newses as $news)
                            <tr>
                                <td colspan="4">
                                    <img src="{{ $news->image_path }}" width="100px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {{ $news->name }}
                                </td>
                                <td>
                                    {!! $types[$news->type] !!}
                                </td>
                                <td>
                                    {!! $news->created_at !!}
                                </td>
                                {{--<td>
                                    <a href="{!! route('admin.news.destroy', [
                                        'user' => $news->getKey()
                                    ]) !!}" onclick="event.preventDefault();document.getElementById('deleteForm').action = this.href;document.getElementById('deleteForm').submit();" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="deleteForm" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="DELETE">
    </form>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
