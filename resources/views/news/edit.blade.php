@extends('layouts.app')
@section('title')Добавление события@endsection
@section('content')
    <form action="{!! route('admin.news.update', [
        'news' => $news->getKey()
    ]) !!}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Добавление события</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Название:</label>
                            <input type="text" name="name" class="form-control" value="{{ $news->name }}">
                        </div>

                        <div class="form-group">
                            <label>Тип:</label>
                            <select class="form-control" name="type">
                                @foreach($types as $key=>$value)
                                    <option value="{!! $key !!}" {!! ($news->type) ? 'selected':'' !!}>{!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Роли:</label>
                            <select multiple class="form-control" name="roles[]">
                                @foreach($roles as $key=>$value)
                                    <option value="{!! $key !!}" {!! in_array($key, (array) $news->roles) ? 'selected':'' !!}>{!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Города:</label>
                            <select multiple class="form-control" name="cities[]">
                                @foreach($cities as $key=>$value)
                                    <option value="{!! $key !!}" {!! in_array($key, (array) $news->cities) ? 'selected':'' !!}>{!! $value->name !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Текст:</label>
                            <textarea class="form-control" name="text">{{ $news->text }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="inputFile">Картинка:</label>
                            <input type="file" name="image" id="inputFile" accept="image/jpeg,image/png">
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
