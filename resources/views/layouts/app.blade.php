<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    @section('page.header.meta')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        @stack('page.header.meta')
    @show
    @section('page.header.styles')
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/template/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/template/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/template/dist/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/template/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="/template/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/template/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="/template/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="/template/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        @stack('page.header.styles')
    @show

    @section('page.header.scripts')
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @stack('page.header.scripts')
    @show
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo hidden-xs">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>ADP</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>{{\App\Helpers\Roles::getName(\Auth::user()->role)}}</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="info">
                        <p>{{ Auth::user()->name }} ({{\App\Helpers\Roles::getName(\Auth::user()->role)}})</p>
                        <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выход</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
                <ul class="sidebar-menu">
                    @if(\Auth::user()->isSuperAdmin())
                    <li class="{!! request()->route()->getName() == 'admin.users.index' ? 'active' : '' !!}">
                        <a href="{!! route('admin.users.index') !!}">
                            <i class="fa fa-users"></i> <span>Администрация</span>
                        </a>
                    </li>
                    @endif
                    <li class="{!! request()->route()->getName() == 'admin.mentors.index' ? 'active' : '' !!}">
                        <a href="{!! route('admin.mentors.index') !!}">
                            <i class="fa fa-users"></i> <span>Наставники</span>
                        </a>
                    </li>
                    <li class="{!! request()->route()->getName() == 'admin.news.index' ? 'active' : '' !!}">
                        <a href="{!! route('admin.news.index') !!}">
                            <i class="fa fa-newspaper-o"></i> <span>События</span>
                        </a>
                    </li>
                    {{--<li class="{!! request()->route()->getName() == 'admin.questionnaire.index' ? 'active' : '' !!}">
                        <a href="{!! route('admin.questionnaire.index') !!}">
                            <i class="fa fa-file-o"></i> <span>Анкеты</span>
                        </a>
                    </li>
                    <li class="{!! request()->route()->getName() == 'admin.reports.index' ? 'active' : '' !!}">
                        <a href="{!! route('admin.reports.index') !!}">
                            <i class="fa fa-file-o"></i> <span>Отчеты</span>
                        </a>
                    </li>--}}
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                @section('content-title')
                    @yield('content-title')
                @show
                @section('breadcrumb')
                    @yield('breadcrumb')
                @show
            </section>

            <!-- Main content -->
            <section class="content">
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="icon fa fa-ban"></i> {!! $error !!}
                        </div>
                    @endforeach
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-ban"></i> {!! session()->get('message') !!}
                    </div>
                @endif
                @section('content')
                    @yield('content')
                @show
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer"></footer>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    @section('page.footer.scripts')
        <!-- jQuery 2.2.3 -->
        <script src="/template/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="/template/bootstrap/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="/template/plugins/morris/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="/template/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="/template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="/template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="/template/plugins/knob/jquery.knob.js"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="/template/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="/template/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="/template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="/template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="/template/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="/template/dist/js/app.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        {{--<script src="/template/dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="/template/dist/js/demo.js"></script>--}}
        @stack('page.footer.scripts')
    @show
</body>
</html>
