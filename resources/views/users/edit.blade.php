@extends('layouts.app')
@section('title')Пользователи@endsection
@section('content')
    <form action="{!! route('admin.users.update', [
        'user' => $user->getKey()
    ]) !!}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Редактирование пользователя</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Имя</label>
                            <input type="text" name="name" class="form-control" value="{{ @old('name') ?? $user->name }}">
                        </div>

                        <div class="form-group">
                            <label>Email:</label>
                            <input type="text" name="email" class="form-control" value="{{ @old('email') ?? $user->email }}">
                        </div>

                        <div class="form-group">
                            <label>Роль:</label>
                            <select class="form-control" name="role">
                                @foreach($roles as $key => $value)
                                    <option value="{!! $key !!}" {!! $user->role == $key ? 'selected':'' !!}>{!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Город:</label>
                            <select class="form-control" name="city_id">
                                @foreach($cities as $key => $value)
                                    <option value="{!! $value->id !!}" {!! $user->city_id == $value->id ? 'selected':'' !!}>{!! $value->name !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Пароль:</label>
                            <input type="password" name="password" class="form-control" value="">
                        </div>

                        <div class="form-group">
                            <label>Подтверждение пароля:</label>
                            <input type="password" name="password_confirmation" class="form-control" value="">
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('page.footer.scripts')
    <script>

    </script>
@endpush
