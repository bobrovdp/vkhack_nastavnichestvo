@extends('layouts.app')
@section('title')Администрация@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Администрация</h3>
                    <a href="{!! route('admin.users.create') !!}" class="btn btn-primary btn-xs pull-right">Добавить</a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Имя</th>
                            {{--<th>Email</th>--}}
                            <th>Город</th>
                            <th>Роль</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <a href="{!! route('admin.users.edit', [
                                        'user' => $user->getKey()
                                    ]) !!}">
                                        {{ $user->name }}
                                    </a>
                                </td>
                                {{--<td>
                                    {{ $user->email }}
                                </td>--}}
                                <td>
                                    {{ $user->city->name }}
                                </td>
                                <td>
                                    {{ \App\Helpers\Roles::getName($user->role) }}
                                </td>
                                <td>
                                    <a href="{!! route('admin.users.edit', [
                                        'user' => $user->getKey()
                                    ]) !!}" class="btn btn-warning">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{!! route('admin.users.destroy', [
                                        'user' => $user->getKey()
                                    ]) !!}" onclick="event.preventDefault();document.getElementById('usersDelete').action = this.href;document.getElementById('usersDelete').submit();" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <form id="usersDelete" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="DELETE">
    </form>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
