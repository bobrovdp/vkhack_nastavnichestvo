@extends('layouts.app')
@section('title')Отчет@endsection
@section('content')
    <form action="{!! route('admin.reports.update', [
        'user' => $report->getKey()
    ]) !!}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Отчет</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Даты:</label> {{ implode(', ', $report->dates) }}
                        </div>

                        <div class="form-group">
                            <label>Чем занимались на встече:</label> {{ $report->description }}
                        </div>

                        <div class="form-group">
                            <label>Оценка настроения ребенка до встречи:</label> {{ $report->rating_children_before }}
                        </div>

                        <div class="form-group">
                            <label>Оценка настроения ребенка после встречи:</label> {{ $report->rating_children_after }}
                        </div>

                        <div class="form-group">
                            <label>Оценка настроения после встречи:</label> {{ $report->rating_after }}
                        </div>

                        <div class="form-group">
                            <label>Цель на следующую встречу :</label> {{ $report->target }}
                        </div>

                        <div class="form-group">
                            <label>Комментарии :</label> {{ $report->comment }}
                        </div>

                        <div class="form-group">
                            <label>Фотографии :</label>
                            @foreach($report->photos as $photo)
                                <img src="{!! $photo; !!}">
                            @endforeach
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
