@extends('layouts.app')
@section('title')Отчеты@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Отчеты</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Тип</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($reports as $report)
                            <tr>
                                <td>
                                    {!! $report->getKey() !!}
                                </td>
                                <td>
                                    <a href="{!! route('admin.reports.edit', [
                                        'user' => $report->getKey()
                                    ]) !!}">
                                        {!! implode(', ', $report->dates) !!}
                                    </a>
                                </td>
                                <td>
                                    <a href="{!! route('admin.reports.edit', [
                                        'user' => $report->getKey()
                                    ]) !!}">
                                        {{ $report->user->profile->name }} {{ $report->user->profile->surname }}
                                    </a>
                                </td>
                                <td>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
