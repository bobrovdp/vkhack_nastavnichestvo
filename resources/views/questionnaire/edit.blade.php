@extends('layouts.app')
@section('title')Анкета@endsection
@section('content')
    <form action="{!! route('admin.questionnaire.update', [
        'user' => $q->getKey()
    ]) !!}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="PUT">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Анкета</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Имя:</label> {{ $q->name }}
                        </div>

                        <div class="form-group">
                            <label>Фамилия:</label> {{ $q->surname }}
                        </div>

                        <div class="form-group">
                            <label>Отчество:</label> {{ $q->patronymic }}
                        </div>

                        <div class="form-group">
                            <label>Статус:</label>
                            <select class="form-control" name="status">
                                @foreach($statuses as $key=>$value)
                                    <option value="{!! $key !!}" {!! $q->status == $key ? 'selected':'' !!}>{!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('page.footer.scripts')
    <script>

    </script>
@endpush
