@extends('layouts.app')
@section('title')Анкета@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Анкета</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ФИО</th>
                            <th>Статус</th>
                            <th>Мотивация</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {{ $user->profile->surname  }} {{ $user->profile->name  }} {{ $user->profile->patronymic  }}
                            </td>
                            <td>
                                {{ $statuses[$user->profile->status]  }}
                            </td>
                            <td>
                                {{ $user->profile->reason }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page.footer.scripts')
    <script></script>
@endpush
