<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/admin/login', ['as' => 'login', 'uses' => 'App\Http\Controllers\Auth\LoginController@showLoginForm']);
Route::post('/admin/login', ['uses' => 'App\Http\Controllers\Auth\LoginController@login']);
Route::post('/admin/logout', ['as' => 'login', 'uses' => 'App\Http\Controllers\Auth\LoginController@logout']);

Route::group(['as' => 'admin.', 'prefix' => '/admin', 'middleware' => ['auth', 'is.admin']], function() {

    Route::get('/', ['as' => 'home', 'uses' => 'Admin\\HomeController@index']);

    Route::group(['as' => 'users.', 'prefix' => '/users'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Admin\\UsersController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => 'Admin\\UsersController@create']);
        Route::get('/{user}', ['as' => 'edit', 'uses' => 'Admin\\UsersController@edit']);
        Route::put('/{user}', ['as' => 'update', 'uses' => 'Admin\\UsersController@update']);
        Route::delete('/{user}', ['as' => 'destroy', 'uses' => 'Admin\\UsersController@destroy']);
        Route::post('/', ['as' => 'store', 'uses' => 'Admin\\UsersController@store']);
    });

    Route::group(['as' => 'mentors.', 'prefix' => '/mentors'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Admin\\MentorsController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => 'Admin\\MentorsController@create']);
        Route::get('/{user}', ['as' => 'edit', 'uses' => 'Admin\\MentorsController@edit']);
        Route::put('/{user}', ['as' => 'update', 'uses' => 'Admin\\MentorsController@update']);
        Route::delete('/{user}', ['as' => 'destroy', 'uses' => 'Admin\\MentorsController@destroy']);
        Route::post('/', ['as' => 'store', 'uses' => 'Admin\\MentorsController@store']);
    });

    Route::group(['as' => 'news.', 'prefix' => '/news'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Admin\\NewsController@index']);
        Route::get('/create', ['as' => 'create', 'uses' => 'Admin\\NewsController@create']);
        Route::post('/', ['as' => 'store', 'uses' => 'Admin\\NewsController@store']);
        Route::get('/{news}', ['as' => 'edit', 'uses' => 'Admin\\NewsController@edit']);
        Route::put('/{news}', ['as' => 'update', 'uses' => 'Admin\\NewsController@update']);
        Route::delete('/{news}', ['as' => 'destroy', 'uses' => 'Admin\\NewsController@destroy']);
    });

    Route::group(['as' => 'questionnaire.', 'prefix' => '/questionnaire'], function() {
        Route::get('/{q}', ['as' => 'index', 'uses' => 'Admin\\QuestionnaireController@index']);
//        Route::get('/{q}', ['as' => 'edit', 'uses' => 'Admin\\QuestionnaireController@edit']);
//        Route::put('/{q}', ['as' => 'update', 'uses' => 'Admin\\QuestionnaireController@update']);
    });

    Route::group(['as' => 'reports.', 'prefix' => '/reports'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Admin\\ReportsController@index']);
        Route::get('/{report}', ['as' => 'edit', 'uses' => 'Admin\\ReportsController@edit']);
        Route::put('/{report}', ['as' => 'update', 'uses' => 'Admin\\ReportsController@update']);
    });
});
