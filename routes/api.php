<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['as' => 'questionnaires.', 'prefix' => '/questionnaires'], function() {
        Route::get('/', ['as' => 'questionnaires.update', 'uses' => 'QuestionnaireController@show']);
        Route::put('/', ['as' => 'questionnaires.update', 'uses' => 'QuestionnaireController@update']);
    });

    Route::group(['as' => 'references.', 'prefix' => '/references'], function() {
        Route::get('/', ['as' => 'reference.create', 'uses' => 'ReferenceController@index']);
        Route::post('/', ['as' => 'references.create', 'uses' => 'ReferenceController@create']);
    });

    Route::group(['as' => 'reports.', 'prefix' => '/reports'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'ReportsController@index']);
        Route::post('/', ['as' => 'create', 'uses' => 'ReportsController@create']);
    });

    Route::group(['as' => 'messages.', 'prefix' => '/messages'], function () {
        Route::post('/', ['as' => 'store', 'uses' => 'MessagesController@store']);
        Route::get('/', ['as' => 'index', 'uses' => 'MessagesController@index']);
    });

    Route::group(['as' => 'news.', 'prefix' => '/news'], function() {
        Route::get('/', ['as' => 'create', 'uses' => 'NewsController@index']);
        Route::post('/', ['as' => 'create', 'uses' => 'NewsController@create']);
    });

    Route::group(['as' => 'projects.', 'prefix' => '/projects'], function() {
        Route::get('/', ['as' => 'create', 'uses' => 'ProjectsController@index']);
        Route::post('/', ['as' => 'create', 'uses' => 'ProjectsController@create']);
    });
});

Route::get('/cities', ['as' => 'cities.index', 'uses' => 'CitiesController@index']);

Route::post('/users', ['as' => 'users.create', 'uses' => 'UsersController@create']);

Route::post('/auth', ['as' => 'users.auth', 'uses' => 'UsersController@apiAuth']);
